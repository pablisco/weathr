package com.pablisco.weathr.persist;

import android.content.SharedPreferences;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.gson.Gson;
import com.koushikdutta.ion.Ion;
import com.pablisco.weathr.concurrent.FutureTask;

import static android.preference.PreferenceManager.getDefaultSharedPreferences;
import static com.pablisco.weathr.WeathrApplication.app;

/**
 * Created by pablisco on 30/05/2014.
 * <p/>
 * This store uses the Shared preferences and Gson to persist data as well as a static cache.
 * This is only useful for small data. For larger datasets we should use a database and content
 * provider. However, this solution is 'good enough' for time constrain tasks :)
 * <p/>
 * TODO: Unit tests
 */
public class SimpleStore {

	/**
	 * This cache is gonna be used to keep objects that are added to the store in case. This allows
	 * for a faster return if the application hasn't run out of memory or being killed. The cache
	 * uses soft references to avoid memory leaks and a limit of 100 entries.
	 */
	private static final Cache<String, Object> mCache = CacheBuilder.newBuilder().softValues().maximumSize(100).build();

	/**
	 * Prefix used to save the entry time stamp to allow the data to be expired
	 */
	public static final String KEY_TIME_STAMP = "TIME_STAMP+";

	/**
	 * Expiration of data for a day. Anything before that will be ignored
	 */
	public static final long DAY_EXPIRATION = 24 * 3600;

	private static SimpleStore sInstance;

	private SimpleStore() {
	}

	public static SimpleStore simpleStore() {
		if (sInstance == null) {
			sInstance = new SimpleStore();
		}
		return sInstance;
	}

	/**
	 * Used for saving multiple items in the save store. This object is sent to the {@link SaveTask}
	 * to be processed.
	 *
	 * @param <T>
	 */
	public static class StoreEntry<T> {

		private final String mKey;

		private final T mObject;

		private StoreEntry(String mKey, T mObject) {
			this.mKey = mKey;
			this.mObject = mObject;
		}
	}

	/**
	 * Future task used to save a set of {@link StoreEntry} instances into the shared preferences and the cache.
	 */
	private class SaveTask extends FutureTask<StoreEntry, Boolean> {

		@Override
		protected Boolean tryInBackground(StoreEntry... params) throws Throwable {
			boolean result = false;
			if (params.length > 0) {
				SharedPreferences.Editor preferences = getDefaultSharedPreferences(app()).edit();
				// load the same gson as in Ion (this would probably be elsewhere if this was going
				// outside of this app)
				Gson gson = Ion.getDefault(app()).configure().getGson();
				long now = System.currentTimeMillis();
				for (StoreEntry entry : params) {
					mCache.put(entry.mKey, entry.mObject);
					String data = gson.toJson(entry.mObject);
					preferences.putString(entry.mKey, data);
					preferences.putLong(KEY_TIME_STAMP + entry.mKey, now);
				}
				result = preferences.commit();
			}
			return result;
		}

	}

	/**
	 * Future task used for restoring data from the shared preferences or the cache if available.
	 *
	 * @param <T>
	 */
	private class RestoreTask<T> extends FutureTask<String, T> {

		private final Class<T> mType;

		/**
		 * How log since the entry was inserted to restore. If this value is lower than 0 then data
		 * is not restored and simply return null (A bit silly but still the possibility is there).
		 * If it is 0 then the data will be preserved for ever (or until the user deletes the app
		 * data). Anything over 0 will only restore the data if the store time stamp is within the
		 * set (now - entryLife, now]
		 */
		private long mEntryLife = 0;

		private RestoreTask(Class<T> mType) {
			this.mType = mType;
		}

		private RestoreTask(Class<T> type, long entryLife) {
			this(type);
			this.mEntryLife = entryLife;
		}

		@Override
		protected T tryInBackground(String... params) throws Throwable {
			T result = null;
			if (params.length > 0) {
				SharedPreferences preferences = getDefaultSharedPreferences(app());
				// we only process one value at a time to be able to use generics
				String key = params[0];
				if (mEntryLife >= 0) {
					long now = System.currentTimeMillis();
					boolean restore = mEntryLife == 0;
					if (!restore) {
						// check if the entry still has life
						final long timestamp = preferences.getLong(KEY_TIME_STAMP + key, Integer.MIN_VALUE);
						restore = timestamp > (now - mEntryLife) && timestamp < now;
					}
					if (restore) {
						// try to get the result from the cache
						//noinspection unchecked
						result = (T) mCache.getIfPresent(key);
						String json;

						if (result == null && preferences.contains(key)
							&& (json = preferences.getString(key, null)) != null) {
							Gson gson = Ion.getDefault(app()).configure().getGson();
							result = gson.fromJson(json, mType);

							mCache.put(key, result);
						}
					}
				}
			}
			return result;
		}
	}

	private static <T> StoreEntry<T> entry(String key, T object) {
		return new StoreEntry<>(key, object);
	}

	/**
	 * @param key
	 * @param object
	 * @param <T>
	 * @return
	 */
	public <T> ListenableFuture<Boolean> save(String key, T object) {
		return save(entry(key, object));
	}

	/**
	 * Saves a given object using the {@link SaveTask}
	 *
	 * @param items
	 * @return
	 */
	@SuppressWarnings("WeakerAccess")
	public ListenableFuture<Boolean> save(StoreEntry<?>... items) {
		return new SaveTask().executeWithFuture(items);
	}

	/**
	 * Restores the object that was saved with the provided key. The type must be provided to cast
	 * the result into the correct format.
	 *
	 * @param key
	 * @param type
	 * @param <T>
	 * @return A future that can be used to get the data when ready.
	 */
	public <T> ListenableFuture<T> restore(String key, Class<T> type) {
		return new RestoreTask<>(type).executeWithFuture(key);
	}

	/**
	 *
	 * Same as {@link #restore(String, Class)} but with the option to add an expiration time in milliseconds
	 *
	 * @param key
	 * @param type
	 * @param entryExpiration
	 * @param <T>
	 * @return
	 */
	public <T> ListenableFuture<T> restore(String key, Class<T> type, long entryExpiration) {
		return new RestoreTask<>(type, entryExpiration).executeWithFuture(key);
	}

}
