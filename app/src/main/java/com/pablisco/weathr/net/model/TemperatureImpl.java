package com.pablisco.weathr.net.model;

import com.google.gson.annotations.SerializedName;
import com.pablisco.weathr.model.Temperature;

/**
 * Created by pablisco on 29/05/2014.
 *
 * Implementation for {@link Temperature}. The temperatures are all in Kelvin unless requested
 * otherwise.
 *
 */
public class TemperatureImpl implements Temperature {

	private Float day;

	@SerializedName("min")
	private Float minimum;

	@SerializedName("max")
	private Float maximum;

	private Float night;

	@SerializedName("eve")
	private Float evening;

	@SerializedName("morn")
	private Float morning;

	@Override
	public Float getDay() {
		return day;
	}

	@Override
	public Float getMinimum() {
		return minimum;
	}

	@Override
	public Float getMaximum() {
		return maximum;
	}

	@Override
	public Float getNight() {
		return night;
	}

	@Override
	public Float getEvening() {
		return evening;
	}

	@Override
	public Float getMorning() {
		return morning;
	}
}
