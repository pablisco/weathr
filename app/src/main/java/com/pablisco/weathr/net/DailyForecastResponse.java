package com.pablisco.weathr.net;

import com.google.common.collect.Iterables;
import com.google.gson.annotations.SerializedName;
import com.pablisco.weathr.model.City;
import com.pablisco.weathr.model.Forecast;
import com.pablisco.weathr.net.model.CityImpl;
import com.pablisco.weathr.net.model.ForecastImpl;

import java.lang.Iterable;
import java.util.Collection;

import static java.util.Collections.emptyList;

/**
 * Created by pablisco on 29/05/2014.
 * <p>
 * Model for the Response recieved from this end-point:
 * <pre>http://api.openweathermap.org/data/2.5/forecast/daily</pre>
 * </p>
 * <p>
 * The minimum parameters are: lat=XX&amp;lon=XX
 * </p>
 */
public class DailyForecastResponse {

	private CityImpl city;

	@SerializedName("cnt")
	private Integer count;

	@SuppressWarnings("CanBeFinal")
	@SerializedName("list")
	private Collection<ForecastImpl> forecastList = emptyList();

	/**
	 * @return The city returned by the API for the provided query
	 */
	public City getCity() {
		return city;
	}

	/**
	 * @return The total number of items provided
	 */
	public Integer getCount() {
		return count;
	}

	/**
	 * @return An {@link Iterable} of {@link Forecast} items with the same size as provided by {@link #getCount()}
	 */
	public Iterable<Forecast> getForecasts() {
		// we use Guava to ensure the right type
		return Iterables.filter(forecastList, Forecast.class);
	}


}
