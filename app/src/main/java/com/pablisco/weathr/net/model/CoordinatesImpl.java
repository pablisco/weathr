package com.pablisco.weathr.net.model;

import com.google.gson.annotations.SerializedName;
import com.pablisco.weathr.model.Coordinates;

/**
 * Created by pablisco on 29/05/2014.
 *
 * Concrete implementation for {@link Coordinates} for:
 * <a href="http://openweathermap.org/API">OpenWeatherMap. API.</a>
 *
 */
public class CoordinatesImpl implements Coordinates {

	@SerializedName("lat")
	private Float latitude;

	@SerializedName("lon")
	private Float longitude;

	@Override
	public Float getLatitude() {
		return latitude;
	}

	@Override
	public Float getLongitude() {
		return longitude;
	}
}
