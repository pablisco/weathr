package com.pablisco.weathr.net.model;

import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.collect.Iterables;
import com.google.gson.annotations.SerializedName;
import com.pablisco.weathr.model.Forecast;
import com.pablisco.weathr.model.Temperature;
import com.pablisco.weathr.model.Weather;

import java.util.Collection;
import java.util.Date;
import java.util.TimeZone;

import hirondelle.date4j.DateTime;

/**
 * Created by pablisco on 29/05/2014.
 *
 * Implementation for {@link Forecast}
 *
 */
public class ForecastImpl implements Forecast {

	@SerializedName("dt")
	private Long time;

	@SerializedName("temp")
	private TemperatureImpl temperature;

	private Float pressure;

	@SerializedName("speed")
	private Float windSpeed;

	@SerializedName("deg")
	private Float windDirection;

	private Integer humidity;

	@SerializedName("weather")
	private Collection<WeatherToken> weatherTokens;

	/**
	 * This class is used to parse the data from the api into a meaningful value. Only the id will
	 * be taken from the object provided.
	 */
	public static class WeatherToken {

		private Integer id;

	}

	@Override
	public DateTime getTime() {
		// time is provided in seconds
		return DateTime.forInstant(time * 1000, TimeZone.getDefault());
	}

	@Override
	public Temperature getTemperature() {
		return temperature;
	}

	@Override
	public Float getPressure() {
		return pressure;
	}

	@Override
	public Float getWindSpeed() {
		return windSpeed;
	}

	@Override
	public Float getWindDirection() {
		return windDirection;
	}

	@Override
	public Integer getHumidity() {
		return humidity;
	}

	@Override
	public Iterable<Weather> getWeatherList() {
		// parse WeatherToken objects into Weather objects
		return Iterables.transform(weatherTokens, new Function<WeatherToken, Weather>() {
			public Weather apply(WeatherToken input) {
				return Weather.from(input.id);
			}
		});
	}

	@Override
	public String toString() {
		return Joiner.on(", ").join(getWeatherList());
	}
}
