package com.pablisco.weathr.net.model;

import com.google.gson.annotations.SerializedName;
import com.pablisco.weathr.model.City;
import com.pablisco.weathr.model.Coordinates;

/**
 * Created by pablisco on 29/05/2014.
 *
 * Concrete implementation for {@link City} for:
 * <a href="http://openweathermap.org/API">OpenWeatherMap. API.</a>
 *
 */
public class CityImpl implements City {

	private Long id;

	private String name;

	@SerializedName("coords")
	private CoordinatesImpl coordinates;

	private String country;

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public Coordinates getCoordinates() {
		return coordinates;
	}

	@Override
	public String getCountry() {
		return country;
	}

}
