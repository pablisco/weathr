package com.pablisco.weathr.net;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader;

import com.google.common.base.Optional;
import com.koushikdutta.ion.Ion;
import com.pablisco.weathr.lang.Either;

import static com.pablisco.weathr.persist.SimpleStore.DAY_EXPIRATION;
import static com.pablisco.weathr.persist.SimpleStore.simpleStore;

/**
 * Created by pablisco on 29/05/2014.
 *
 * This {@link Loader} is in charge of loading data from the url at {@link #mEndpoint}
 *
 */
public class DailyForecastLoader extends AsyncTaskLoader<Either<DailyForecastResponse, Throwable>> {

	private static final String KEY_LATITUDE = "lat";
	private static final String KEY_LONGITUDE = "lon";
	private static final String KEY_COUNT = "cnt";
	private static final String KEY_LOCATION_NAME = "q";
	private static final String KEY_LOCATION_ID = "id";

	private static final Uri mEndpoint = Uri.parse("http://api.openweathermap.org/data/2.5/forecast/daily");

	private Optional<Double> mLongitude = Optional.absent();
	private Optional<Double> mLatitude = Optional.absent();
	private Optional<Integer> mCount = Optional.absent();
	private Optional<String> mLocationName = Optional.absent();
	private Optional<Integer> mLocationId = Optional.absent();

	private DailyForecastLoader(Context context) {
		super(context);
	}

	/**
	 * Static constructor for {@link DailyForecastLoader}
	 * @param context context for loader
	 * @return An instance of {@link DailyForecastLoader}
	 */
	public static DailyForecastLoader from(Context context) {
		return new DailyForecastLoader(context);
	}

	/* Withers */

	public DailyForecastLoader withLocation(Double longitude, Double latitude) {
		this.mLongitude = Optional.fromNullable(longitude);
		this.mLatitude = Optional.fromNullable(latitude);
		return this;
	}

	public DailyForecastLoader withCount(Integer count) {
		this.mCount = Optional.fromNullable(count);
		return this;
	}

	public DailyForecastLoader withLocationName(String mLocationName) {
		this.mLocationName = Optional.fromNullable(mLocationName);
		return this;
	}

	public DailyForecastLoader withLocationId(Integer mLocationId) {
		this.mLocationId = Optional.fromNullable(mLocationId);
		return this;
	}

	/**
	 * TODO: Validate combinations of parameters
	 *
	 * This method will try to add the parameters to a newly create copy of the {@link #mEndpoint}
	 *
	 * @return the uri with the parameters needed
	 */
	public Uri getQuery() {
		Uri.Builder query = mEndpoint.buildUpon();
		if (mLatitude.isPresent()) {
			query.appendQueryParameter(KEY_LATITUDE, mLatitude.get().toString());
		}
		if (mLongitude.isPresent()) {
			query.appendQueryParameter(KEY_LONGITUDE, mLongitude.get().toString());
		}
		if (mCount.isPresent()) {
			query.appendQueryParameter(KEY_COUNT, mCount.get().toString());
		}
		if (mLocationName.isPresent()) {
			query.appendQueryParameter(KEY_LOCATION_NAME, mLocationName.get());
		}
		if (mLocationId.isPresent()) {
			query.appendQueryParameter(KEY_LOCATION_ID, mLocationId.get().toString());
		}
		return query.build();
	}

	/**
	 *
	 * Quick Google to get this: http://stackoverflow.com/a/4239019/458365
	 *
	 * @return whether there is a connection available
	 */
	private boolean isNetworkAvailable() {
		ConnectivityManager connectivityManager
			= (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
		return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	}

	@Override
	public Either<DailyForecastResponse, Throwable> loadInBackground() {
		Either<DailyForecastResponse, Throwable> result;
		try {
			String url = getQuery().toString();
			DailyForecastResponse response;
			if (isNetworkAvailable()) {
				response = Ion.with(getContext())
					.load(url)
					.as(DailyForecastResponse.class)
						// This is safe, we should be in the background
					.get();
				simpleStore().save(url, response);
			} else {
				response = simpleStore().restore(url, DailyForecastResponse.class, DAY_EXPIRATION).get();
			}
			if (response != null) {
				result = Either.left(response);
			} else {
				throw new NotInternetException();
			}
		} catch (Throwable e) {
			result = Either.right(e);
		}
		return result;
	}
}
