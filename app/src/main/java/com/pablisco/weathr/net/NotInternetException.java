package com.pablisco.weathr.net;

import com.pablisco.weathr.persist.SimpleStore;

import java.io.IOException;

/**
 * Created by pablisco on 31/05/2014.
 *
 * Exception thrown when the application can't reach the internet or restore data from the
 * {@link SimpleStore}
 *
 */
public class NotInternetException extends IOException {
}
