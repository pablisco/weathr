/**
 * Model for the network layer. Mostly concrete implementations of the application model.
 */
package com.pablisco.weathr.net.model;