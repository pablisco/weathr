package com.pablisco.weathr.adapters;

import com.pablisco.weathr.R;
import com.pablisco.weathr.lang.Either;

import java.util.Locale;
import java.util.TimeZone;

import hirondelle.date4j.DateTime;

/**
 * This enum provides with the different types of date groups as well as the functionality to
 * factory them from a {@link DateTime} instance. This library is used because it provides
 * useful functionality without adding too much overhead (like in teh case of Yota time). Date4J
 * is basically a one class library. The only improvements this library could have it to have
 * more {@link DateTime#plusDays(Integer)} like methods and confront with Java formating standards.
 */
public enum DateTimeThreshold {
	TODAY, TOMORROW, THIS_WEEK, LATER;

	public static DateTimeThreshold from(DateTime date) {
		TimeZone timeZone = TimeZone.getDefault();
		DateTimeThreshold results = LATER;
		DateTime today = DateTime.today(timeZone);
		DateTime thisWeek = today.getEndOfDay().plusDays(7);
		if (date.isSameDayAs(DateTime.today(timeZone))) {
			results = TODAY;
		} else if(date.isSameDayAs(today.plusDays(1))) {
			results = TOMORROW;
		} else if(date.lt(thisWeek)) {
			results = THIS_WEEK;
		}
		return results;
	}

	/**
	 * Converts a {@link DateTime} instance into either a tet resource or some actual text.
	 *
	 * @param date
	 * @return
	 */
	public static Either<Integer, String> asText(DateTime date) {
		Either<Integer, String> dateText;

		switch (DateTimeThreshold.from(date)) {
			case TODAY:
				dateText = Either.left(R.string.today);
				break;
			case TOMORROW:
				dateText = Either.left(R.string.tomorrow);
				break;
			case THIS_WEEK:
				dateText = Either.right(date.format("WWWW", Locale.getDefault()));
				break;
			case LATER:
			default:
				dateText = Either.right(date.format("WWW D MMM", Locale.getDefault()));
				break;
		}

		return dateText;
	}

}
