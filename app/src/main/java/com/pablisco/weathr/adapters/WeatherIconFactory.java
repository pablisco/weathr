package com.pablisco.weathr.adapters;

import com.pablisco.weathr.R;
import com.pablisco.weathr.model.Weather;

import java.util.Map;

/**
 * Created by pablisco on 31/05/2014.
 *
 * Factory used to convert a {@link Weather} instance into a drawable.
 *
 */
public class WeatherIconFactory {

	public static int weatherIconFrom(Weather weather) {
		return weatherIconFrom(weather, false);
	}

	public static int weatherIconFrom(Weather weather, boolean large) {

		int result = 0;

		switch (weather) {

			case NONE:
				break;
			case THUNDERSTORM_WITH_LIGHT_RAIN:
			case THUNDERSTORM_WITH_RAIN:
			case THUNDERSTORM_WITH_HEAVY_RAIN:
			case LIGHT_THUNDERSTORM:
			case THUNDERSTORM:
			case HEAVY_THUNDERSTORM:
			case RAGGED_THUNDERSTORM:
			case THUNDERSTORM_WITH_LIGHT_DRIZZLE:
			case THUNDERSTORM_WITH_DRIZZLE:
			case THUNDERSTORM_WITH_HEAVY_DRIZZLE:
			case LIGHT_INTENSITY_DRIZZLE:
				result = (large) ? R.drawable.ic_weather_thunderstorms_large : R.drawable.ic_weather_thunderstorms;
				break;
			case DRIZZLE:
			case HEAVY_INTENSITY_DRIZZLE:
			case LIGHT_INTENSITY_DRIZZLE_RAIN:
			case DRIZZLE_RAIN:
			case HEAVY_INTENSITY_DRIZZLE_RAIN:
			case SHOWER_RAIN_AND_DRIZZLE:
			case HEAVY_SHOWER_RAIN_AND_DRIZZLE:
			case SHOWER_DRIZZLE:
				result = (large) ? R.drawable.ic_weather_drizzle_large : R.drawable.ic_weather_drizzle;
				break;
			case LIGHT_RAIN:
			case MODERATE_RAIN:
			case HEAVY_INTENSITY_RAIN:
			case VERY_HEAVY_RAIN:
			case EXTREME_RAIN:
			case FREEZING_RAIN:
			case LIGHT_INTENSITY_SHOWER_RAIN:
			case SHOWER_RAIN:
			case HEAVY_INTENSITY_SHOWER_RAIN:
			case RAGGED_SHOWER_RAIN:
				result = (large) ? R.drawable.ic_weather_drizzle_large : R.drawable.ic_weather_drizzle;
				break;
			case LIGHT_SNOW:
			case SNOW:
			case HEAVY_SNOW:
			case SLEET:
			case SHOWER_SLEET:
			case LIGHT_RAIN_AND_SNOW:
			case RAIN_AND_SNOW:
			case LIGHT_SHOWER_SNOW:
			case SHOWER_SNOW:
			case HEAVY_SHOWER_SNOW:
				result = (large) ? R.drawable.ic_weather_snow_large : R.drawable.ic_weather_snow;
				break;
			case ATMOSPHERE_MIST:
			case ATMOSPHERE_SMOKE:
			case ATMOSPHERE_HAZE:
			case ATMOSPHERE_SAND_OR_DUST_WHIRLS:
			case ATMOSPHERE_FOG:
			case ATMOSPHERE_SAND:
			case ATMOSPHERE_DUST:
			case ATMOSPHERE_VOLCANIC_ASH:
			case ATMOSPHERE_SQUALLS:
			case ATMOSPHERE_TORNADO:
				break;
			case CLOUDS_SKY_IS_CLEAR:
				result = (large) ? R.drawable.ic_weather_sunny_large : R.drawable.ic_weather_sunny;
				break;
			case CLOUDS_FEW_CLOUDS:
				result = (large) ? R.drawable.ic_weather_mostly_cloudy_large : R.drawable.ic_weather_mostly_cloudy;
				break;
			case CLOUDS_SCATTERED_CLOUDS:
			case CLOUDS_BROKEN_CLOUDS:
			case CLOUDS_OVERCAST_CLOUDS:
				result = (large) ? R.drawable.ic_weather_cloudy_large : R.drawable.ic_weather_cloudy;
				break;
			case EXTREME_TORNADO:
			case EXTREME_TROPICAL_STORM:
			case EXTREME_HURRICANE:
			case EXTREME_COLD:
			case EXTREME_HOT:
			case EXTREME_WINDY:
			case EXTREME_HAIL:
				// TODO Sort out extreme icons
				break;
			case ADDITIONAL_SETTING:
			case ADDITIONAL_CALM:
			case ADDITIONAL_LIGHT_BREEZE:
			case ADDITIONAL_GENTLE_BREEZE:
			case ADDITIONAL_MODERATE_BREEZE:
			case ADDITIONAL_FRESH_BREEZE:
			case ADDITIONAL_STRONG_BREEZE:
			case ADDITIONAL_HIGH_WIND_NEAR_GALE:
			case ADDITIONAL_GALE:
			case ADDITIONAL_SEVERE_GALE:
			case ADDITIONAL_STORM:
			case ADDITIONAL_VIOLENT_STORM:
			case ADDITIONAL_HURRICANE:
				// TODO Sort out aditional icons
				break;
		}

		return result;

	}

}
