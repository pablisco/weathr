package com.pablisco.weathr.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.common.base.Joiner;
import com.google.common.base.Preconditions;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.Iterables;
import com.pablisco.weathr.R;
import com.pablisco.weathr.lang.Either;
import com.pablisco.weathr.model.Forecast;
import com.pablisco.weathr.model.Weather;

import java.util.TimeZone;

import hirondelle.date4j.DateTime;

/**
 * Created by pablisco on 31/05/2014.
 *
 * This adapter provides the list fragment with teh views to show from a set of {@link Forecast} instances
 *
 */
public class ForecastAdapter extends BaseAdapter {

	private FluentIterable<Forecast> mForecasts;

	/**
	 * This is used to modify the Kelvin provided values into Celcius (Farenheit will be for version 2.0)
	 */
	public static final float KELVIN_ZERO = 273.15f;

	private ForecastAdapter(Iterable<Forecast> forecasts) {
		super();
		mForecasts = FluentIterable.from(forecasts);
	}

	public static ForecastAdapter from(Iterable<Forecast> forecasts) {
		return new ForecastAdapter(forecasts);
	}

	@Override
	public int getCount() {
		return mForecasts.size();
	}

	@Override
	public Forecast getItem(int position) {
		return mForecasts.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	private static class ViewHolder {

		TextView mDateView;
		TextView mDescriptionView;
		TextView mTemperatureView;
		ImageView mIconView;

	}

	private ViewHolder holderFor(View view) {
		ViewHolder holder = new ViewHolder();
		holder.mDateView = (TextView) view.findViewById(R.id.date_view);
		holder.mDescriptionView = (TextView) view.findViewById(R.id.description_view);
		holder.mTemperatureView = (TextView) view.findViewById(R.id.temperature_view);
		holder.mIconView = (ImageView) view.findViewById(R.id.icon_view);
		return holder;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		Preconditions.checkNotNull(parent, "we require a parent");
		Context context = parent.getContext();
		LayoutInflater inflater = LayoutInflater.from(context);
		View result = convertView;
		ViewHolder holder;
		if (result == null || result.getTag() == null || result.getTag() instanceof ViewHolder) {
			result = inflater.inflate(R.layout.fragment_forecast_list_item, parent, false);
			holder = holderFor(result);
		} else {
			holder = (ViewHolder) result.getTag();
		}
		Forecast forecast = getItem(position);

		float temperatureKelvin = forecast.getTemperature().getDay() - KELVIN_ZERO;
		Either<Integer, String> dateText = DateTimeThreshold.asText(forecast.getTime());

		String temperature = context.getString(R.string.temperature_template, temperatureKelvin);

		DateTime.today(TimeZone.getDefault());

		if (dateText.isLeft()) {
			holder.mDateView.setText(dateText.getLeft());
		} else {
			holder.mDateView.setText(dateText.getRight());
		}

		holder.mTemperatureView.setText(temperature);

		int forecastIcon = 0;
		Iterable<Weather> weatherList = forecast.getWeatherList();
		if (!Iterables.isEmpty(weatherList)) {
			Weather weather = weatherList.iterator().next();
			forecastIcon = WeatherIconFactory.weatherIconFrom(weather);
		}
		holder.mIconView.setImageResource(forecastIcon);

		String description = Joiner.on(", ").join(weatherList);
		holder.mDescriptionView.setText(description);

		return result;
	}



}
