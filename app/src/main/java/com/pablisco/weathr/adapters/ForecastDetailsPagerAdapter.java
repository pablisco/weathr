package com.pablisco.weathr.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;

import com.google.common.collect.FluentIterable;
import com.pablisco.weathr.fragments.ForecastDetailFragment;
import com.pablisco.weathr.fragments.ForecastItemFragment;
import com.pablisco.weathr.lang.Either;
import com.pablisco.weathr.model.Forecast;

import hirondelle.date4j.DateTime;

/**
 * Created by pablisco on 01/06/2014.
 *
 * {@link PagerAdapter} used to generate the fragments for a list of Forecasts
 *
 */
public class ForecastDetailsPagerAdapter extends FragmentStatePagerAdapter {

	private ForecastDetailFragment fragment;
	private final FluentIterable<Forecast> data;

	public ForecastDetailsPagerAdapter(ForecastDetailFragment fragment, FluentIterable<Forecast> data) {
		super(fragment.getChildFragmentManager());
		this.fragment = fragment;
		this.data = data;
	}

	@Override
	public Fragment getItem(int position) {
		return ForecastItemFragment.from(data.get(position));
	}

	@Override
	public int getCount() {
		return data.size();
	}

	@Override
	public CharSequence getPageTitle(int position) {
		CharSequence result;
		DateTime time = data.get(position).getTime();
		Either<Integer, String> dateText = DateTimeThreshold.asText(time);
		if (dateText.isLeft()) {
			result = fragment.getString(dateText.getLeft());
		} else {
			result = dateText.getRight();
		}
		return result;
	}
}
