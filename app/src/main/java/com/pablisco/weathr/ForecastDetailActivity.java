package com.pablisco.weathr;

import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NavUtils;
import android.view.MenuItem;

import com.pablisco.weathr.R;
import com.pablisco.weathr.fragments.ForecastDetailFragment;


/**
 * An activity representing the detailed view of the Forecasts. It simply displays and instance of
 * {@link ForecastDetailFragment} and handles the up button form the action bar. It forwards the
 * extras from the activity into the enclosing fargment.
 */
@SuppressWarnings("WeakerAccess")
public class ForecastDetailActivity extends FragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forecast_detail);

        // Show the Up button in the action bar.
        getActionBar().setDisplayHomeAsUpEnabled(true);

        if (savedInstanceState == null) {
            // Create the detail fragment and add it to the activity
            // using a fragment transaction.
            ForecastDetailFragment fragment = new ForecastDetailFragment();
	        fragment.setArguments(getIntent().getExtras());
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.forecast_detail_container, fragment)
                    .commit();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            NavUtils.navigateUpTo(this, new Intent(this, ForecastListActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
