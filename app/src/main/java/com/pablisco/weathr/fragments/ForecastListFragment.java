package com.pablisco.weathr.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.IntentSender;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.SearchView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.LocationClient;
import com.google.common.base.Strings;
import com.pablisco.weathr.R;
import com.pablisco.weathr.adapters.ForecastAdapter;
import com.pablisco.weathr.lang.Either;
import com.pablisco.weathr.model.Forecast;
import com.pablisco.weathr.net.DailyForecastLoader;
import com.pablisco.weathr.net.DailyForecastResponse;

/**
 * A list fragment representing a list of Forecasts. This fragment
 * also supports tablet devices by allowing list items to be given an
 * 'activated' state upon selection. This helps indicate which item is
 * currently being viewed in a {@link ForecastDetailFragment}.
 * <p/>
 * Activities containing this fragment MUST implement the {@link Callbacks}
 * interface.
 *<p/>
 * This fargment uses Play Services to access the location of the device. Due to time restrains
 * the lack of Play Services is not fully tested.
 */
public class ForecastListFragment extends ListFragment
	implements LoaderManager.LoaderCallbacks<Either<DailyForecastResponse, Throwable>> {

	/**
	 * Define a request code to send to Google Play services
	 * This code is returned in Activity.onActivityResult
	 */
	private final static int
		CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
	private static final String TAG = ForecastListFragment.class.getSimpleName();
	private static final String STATE_LOCATION_NAME = "locationName";

	private LocationClient mLocationClient;

	private static Location sCurrentLocation;

	private String mLocationName;

	/**
	 * The serialization (saved instance state) Bundle key representing the
	 * activated item position. Only used on tablets.
	 */
	private static final String STATE_ACTIVATED_POSITION = "activated_position";

	/**
	 * The fragment's current callback object, which is notified of list item
	 * clicks.
	 */
	private Callbacks mCallbacks = Callbacks.EMPTY;

	/**
	 * The current activated item position. Only used on tablets.
	 */
	private int mActivatedPosition = ListView.INVALID_POSITION;
	private ForecastAdapter mAdapter;

	private Uri mCurrentQuery;
	private SearchView mActionView;
	private MenuItem mHereItem;

	private class LocationCallbacks implements GooglePlayServicesClient.ConnectionCallbacks,
		GooglePlayServicesClient.OnConnectionFailedListener {

		@Override
		public void onConnected(Bundle bundle) {
			Log.d(TAG, "Connected to Play services");
			sCurrentLocation = mLocationClient.getLastLocation();
			refresh();
		}

		@Override
		public void onDisconnected() {
			Log.d(TAG, "Disconnected from Play services. Please re-connect.");
		}

		@Override
		public void onConnectionFailed(ConnectionResult connectionResult) {
			if (connectionResult.hasResolution()) {
				try {
					// Start an Activity that tries to resolve the error
					connectionResult.startResolutionForResult(
						getActivity(),
						CONNECTION_FAILURE_RESOLUTION_REQUEST);
                /*
                 * Thrown if Google Play services canceled the original
                 * PendingIntent
                 */
				} catch (IntentSender.SendIntentException e) {
					// Log the error
					e.printStackTrace();
				}
			} else {
            /*
             * If no resolution is available, display a dialog to the
             * user with the error.
             */
				showErrorDialog(connectionResult.getErrorCode());
			}
		}
	}

	private void showErrorDialog(int errorCode) {
		// Get the error dialog from Google Play services
		Dialog errorDialog = GooglePlayServicesUtil.getErrorDialog(
			errorCode, getActivity(), CONNECTION_FAILURE_RESOLUTION_REQUEST);

		// If Google Play services can provide an error dialog
		if (errorDialog != null) {
			// Create a new DialogFragment for the error dialog
			ErrorDialogFragment errorFragment = new ErrorDialogFragment();
			// Set the dialog in the DialogFragment
			errorFragment.setDialog(errorDialog);
			// Show the error dialog in the DialogFragment
			errorFragment.show(getFragmentManager(),
				"Location Updates");
		}
	}

	private LocationCallbacks mLocationCallbacks = new LocationCallbacks();

	/**
	 * A callback interface that all activities containing this fragment must
	 * implement. This mechanism allows activities to be notified of item
	 * selections.
	 */
	public interface Callbacks {
		/**
		 * Callback for when an item has been selected.
		 *
		 * @param query
		 * @param position
		 * @param forecast
		 */
		public void onItemSelected(String query, Integer position, Forecast forecast);

		/**
		 * An empty implementation of the {@link Callbacks} interface that does
		 * nothing. Used only when this fragment is not attached to an activity.
		 */
		static final Callbacks EMPTY = new Callbacks() {
			@Override
			public void onItemSelected(String query, Integer position, Forecast forecast) {

			}
		};

	}

	/**
	 * Mandatory empty constructor for the fragment manager to instantiate the
	 * fragment (e.g. upon screen orientation changes).
	 */
	public ForecastListFragment() {
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
		mLocationClient = new LocationClient(getActivity(), mLocationCallbacks, mLocationCallbacks);
	}

	@Override
	public void onStart() {
		super.onStart();
		if (mAdapter == null) {
			mLocationClient.connect();
			if (sCurrentLocation == null || mLocationName == null) {
				setListShown(false);
			} else {
				refresh();
			}
		}
	}

	@Override
	public void onStop() {
		if (mLocationClient.isConnected()) {
			mLocationClient.disconnect();
		}
		super.onStop();
	}

	@Override
	public void onResume() {
		super.onResume();

	}

	/**
	 * Method used to load or reload the data. This will triger the loader to be initialised and
	 * the state of the ListFragment set to loading.
	 */
	private void refresh() {
		getActivity().getActionBar().setTitle(R.string.app_name);
		getLoaderManager().restartLoader(0, null, this).forceLoad();
		setListShown(false);
	}

	@Override
	public DailyForecastLoader onCreateLoader(int id, Bundle args) {
		DailyForecastLoader loader = DailyForecastLoader.from(getActivity());
		if(mLocationName != null) {
			loader.withLocationName(mLocationName);
		} else if (sCurrentLocation != null) {
			loader.withLocation(sCurrentLocation.getLongitude(), sCurrentLocation.getLatitude());
		} else {
			throw new IllegalStateException("We should check the location before loading");
		}
		return loader.withCount(16);
	}

	@Override
	public void onLoadFinished(Loader<Either<DailyForecastResponse, Throwable>> loader, Either<DailyForecastResponse, Throwable> either) {
		if (either.isLeft()) {
			// save current query if new data is available
			if (loader instanceof DailyForecastLoader) {
				DailyForecastLoader forecastLoader = (DailyForecastLoader) loader;
				mCurrentQuery = forecastLoader.getQuery();
			}
			DailyForecastResponse response = either.getLeft();

			if (response.getCity() != null && response.getCity().getName() != null) {
				getActivity().getActionBar().setTitle(response.getCity().getName());
			}

			Iterable<Forecast> forecasts = response.getForecasts();

			mAdapter = ForecastAdapter.from(forecasts);

			setListAdapter(mAdapter);

			if (isResumed()) {
				setListShown(true);
			} else {
				setListShownNoAnimation(true);
			}
		} else if (either.isRight()) {
			Throwable error = either.getRight();
			error.printStackTrace();
			Log.d(TAG, error.getMessage() != null ? error.getMessage() : error.toString());
			setListAdapter(null);
		} else {
			throw new IllegalStateException("This shouldn't happen");
		}

		if (getListAdapter() == null || getListAdapter().isEmpty()) {
			String emptyText;
			if (either.isLeft() && mLocationName != null) {
				emptyText = getString(R.string.fragment_forecast_list_error_not_found, mLocationName);
			} else {
				emptyText = getString(R.string.fragment_forecast_list_error);
			}
			setEmptyText(emptyText);
		}
	}

	@Override
	public void onLoaderReset(Loader<Either<DailyForecastResponse, Throwable>> loader) {
		setListAdapter(null);
		if (getView() != null) {
			setListShown(false);
		}
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		// Restore the previously serialized activated item position.
		if (savedInstanceState != null) {
			if (savedInstanceState.containsKey(STATE_ACTIVATED_POSITION)) {
				setActivatedPosition(savedInstanceState.getInt(STATE_ACTIVATED_POSITION));
			}
			mLocationName = savedInstanceState.getString(STATE_LOCATION_NAME);
		}
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		// Activities containing this fragment must implement its callbacks.
		if (!(activity instanceof Callbacks)) {
			throw new IllegalStateException("Activity must implement fragment's callbacks.");
		}

		mCallbacks = (Callbacks) activity;
	}

	@Override
	public void onDetach() {
		super.onDetach();
		// Reset the active callbacks interface to the empty implementation.
		mCallbacks = Callbacks.EMPTY;
	}

	@Override
	public void onListItemClick(ListView listView, View view, int position, long id) {
		super.onListItemClick(listView, view, position, id);

		// Only notify if the is a current query
		if (mCurrentQuery != null) {
			String query = mCurrentQuery.toString();
			mCallbacks.onItemSelected(query, position, mAdapter.getItem(position));
		}
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		if (mActivatedPosition != ListView.INVALID_POSITION) {
			// Serialize and persist the activated item position.
			outState.putInt(STATE_ACTIVATED_POSITION, mActivatedPosition);
			outState.putString(STATE_LOCATION_NAME, mLocationName);
		}
	}

	/**
	 * Turns on activate-on-click mode. When this mode is on, list items will be
	 * given the 'activated' state when touched.
	 */
	public void setActivateOnItemClick(boolean activateOnItemClick) {
		// When setting CHOICE_MODE_SINGLE, ListView will automatically
		// give items the 'activated' state when touched.
		getListView().setChoiceMode(activateOnItemClick
			? ListView.CHOICE_MODE_SINGLE
			: ListView.CHOICE_MODE_NONE);
	}

	private void setActivatedPosition(int position) {
		if (position == ListView.INVALID_POSITION) {
			getListView().setItemChecked(mActivatedPosition, false);
		} else {
			getListView().setItemChecked(position, true);
		}
		mActivatedPosition = position;
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.menu_main, menu);
		mHereItem = menu.findItem(R.id.action_here);
		final MenuItem item = menu.findItem(R.id.action_search);
		mActionView = (SearchView) item.getActionView();
		mActionView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
			@Override
			public boolean onQueryTextSubmit(String query) {
				item.collapseActionView();
				mLocationName = Strings.emptyToNull(query);
				mHereItem.setVisible(true);
				refresh();
				return false;
			}

			@Override
			public boolean onQueryTextChange(String newText) {
				return false;
			}
		});
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		boolean consumed = false;
		int itemId = item.getItemId();
		if (itemId == R.id.action_refresh) {
			setListAdapter(null);
			setListShown(false);
			if (mLocationName == null) {
				if (mLocationClient.isConnected()) {
					sCurrentLocation = mLocationClient.getLastLocation();
					getLoaderManager().restartLoader(0, null, this).forceLoad();
				} else {
					mLocationClient.connect();
				}
			} else {
				getLoaderManager().restartLoader(0, null, this).forceLoad();
			}
			consumed = true;
		} else if(itemId == R.id.action_here) {
			mLocationName = null;
			getActivity().getActionBar().setTitle(R.string.app_name);
			mHereItem.setVisible(false);
			setListAdapter(null);
			setListShown(false);
			getLoaderManager().restartLoader(0, null, this).forceLoad();
		}
		if (!consumed) {
			consumed = super.onOptionsItemSelected(item);
		}
		return consumed;

	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == Activity.RESULT_OK) {
			switch (requestCode) {
				case CONNECTION_FAILURE_RESOLUTION_REQUEST:
					// TODO: handle no play services
					break;
			}
		}
	}

}
