package com.pablisco.weathr.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.common.base.Joiner;
import com.google.common.base.Preconditions;
import com.google.common.collect.Iterables;
import com.pablisco.weathr.R;
import com.pablisco.weathr.adapters.WeatherIconFactory;
import com.pablisco.weathr.model.Forecast;
import com.pablisco.weathr.model.Temperature;
import com.pablisco.weathr.model.Weather;

import static com.pablisco.weathr.adapters.ForecastAdapter.KELVIN_ZERO;

/**
 * Created by pablisco on 31/05/2014.
 *
 * Fragment that displays the details of a single forecast.
 *
 */
public class ForecastItemFragment extends Fragment {

	private Forecast mForecast;

	public static ForecastItemFragment from(Forecast forecast) {
		return new ForecastItemFragment().withForecast(forecast);
	}

	public ForecastItemFragment withForecast(Forecast forecast) {
		this.mForecast = forecast;
		return this;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_forecast_details_item, container, false);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		Preconditions.checkState(mForecast != null, "We must have a Forecasst object");

		TextView conditionsView = (TextView) view.findViewById(R.id.conditions_view);
		TextView temperatureDay = (TextView) view.findViewById(R.id.temperature_day_view);
		TextView temperatureMorning = (TextView) view.findViewById(R.id.temperature_morning_view);
		TextView temperatureMax = (TextView) view.findViewById(R.id.temperature_maximum_view);
		TextView temperatureMin = (TextView) view.findViewById(R.id.temperature_minimum_view);
		TextView temperatureEvening = (TextView) view.findViewById(R.id.temperature_evening_view);
		TextView temperatureNight = (TextView) view.findViewById(R.id.temperature_night_view);
		TextView humidityView = (TextView) view.findViewById(R.id.humidity_view);
		TextView pressureView = (TextView) view.findViewById(R.id.preassure_view);
		TextView windSpeedView = (TextView) view.findViewById(R.id.wind_speed_view);
		TextView windDirectionView = (TextView) view.findViewById(R.id.wind_direction_view);

		ImageView iconView = (ImageView) view.findViewById(R.id.icon_view);

		Iterable<Weather> weatherList = mForecast.getWeatherList();
		String condition = Joiner.on(", ").join(weatherList);

		conditionsView.setText(condition);

		Temperature temperature = mForecast.getTemperature();

		temperatureDay.setText(getString(R.string.temperature_template, temperature.getDay() - KELVIN_ZERO));
		temperatureMorning.setText(getString(R.string.temperature_template, temperature.getMorning() - KELVIN_ZERO));
		temperatureMax.setText(getString(R.string.temperature_template, temperature.getMaximum() - KELVIN_ZERO));
		temperatureMin.setText(getString(R.string.temperature_template, temperature.getMinimum() - KELVIN_ZERO));
		temperatureEvening.setText(getString(R.string.temperature_template, temperature.getEvening() - KELVIN_ZERO));
		temperatureNight.setText(getString(R.string.temperature_template, temperature.getNight() - KELVIN_ZERO));

		humidityView.setText(mForecast.getHumidity() + "%");

		pressureView.setText(mForecast.getPressure() + " mb");

		windSpeedView.setText(mForecast.getWindSpeed() + " mps");

		windDirectionView.setText(getString(R.string.degree_template, mForecast.getWindDirection()));

		int forecastIcon = 0;
		if (!Iterables.isEmpty(weatherList)) {
			Weather weather = weatherList.iterator().next();
			forecastIcon = WeatherIconFactory.weatherIconFrom(weather, true);
		}
		iconView.setImageResource(forecastIcon);

	}
}
