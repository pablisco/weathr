package com.pablisco.weathr.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.astuetz.PagerSlidingTabStrip;
import com.google.common.base.Preconditions;
import com.google.common.collect.FluentIterable;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.pablisco.weathr.R;
import com.pablisco.weathr.adapters.ForecastDetailsPagerAdapter;
import com.pablisco.weathr.model.Forecast;
import com.pablisco.weathr.net.DailyForecastResponse;

import static com.pablisco.weathr.persist.SimpleStore.DAY_EXPIRATION;
import static com.pablisco.weathr.persist.SimpleStore.simpleStore;

/**
 * A fragment representing a single Forecast detail screen.
 * This fragment is either contained in a {@link com.pablisco.weathr.ForecastListActivity}
 * in two-pane mode (on tablets) or a {@link com.pablisco.weathr.ForecastDetailActivity}
 * on handsets.
 */
public class ForecastDetailFragment extends Fragment {

	/**
	 * Key used to provide the fragment with the query that got us here.
	 */
	public static final String ARG_QUERY = "query";

	/**
	 * Key used to provide the position that teh user selected.
	 */
	public static final String ARG_POSITION = "position";

	/**
	 * The query fetched from the arguments
	 */
	private String mQuery;

	/**
	 * The position to start at
	 */
	private Integer mPosition;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ForecastDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
	    Preconditions.checkArgument(getArguments().containsKey(ARG_QUERY));
		mQuery = getArguments().getString(ARG_QUERY);
	    mPosition = getArguments().getInt(ARG_POSITION);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        final View resultView = inflater.inflate(R.layout.fragment_forecast_detail, container, false);

	    ListenableFuture<DailyForecastResponse> future;
	    future = simpleStore().restore(mQuery, DailyForecastResponse.class, DAY_EXPIRATION);

	    Futures.addCallback(future, new FutureCallback<DailyForecastResponse>() {
		    @Override
		    public void onSuccess(DailyForecastResponse result) {
			    final FluentIterable<Forecast> data = FluentIterable.from(result.getForecasts());
			    PagerAdapter adapter = new ForecastDetailsPagerAdapter(ForecastDetailFragment.this, data);
			    final ViewPager pager = (ViewPager) resultView.findViewById(R.id.pager);
			    if (pager != null) {
			        pager.setAdapter(adapter);
			        pager.setCurrentItem(mPosition);
				    PagerSlidingTabStrip tabStrip = (PagerSlidingTabStrip) resultView.findViewById(R.id.tabs);
				    tabStrip.setViewPager(pager);
			    }
		    }

		    @Override
		    public void onFailure(Throwable t) {
				// TODO
		    }
	    });
        return resultView;
    }

}
