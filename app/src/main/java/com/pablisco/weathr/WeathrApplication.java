package com.pablisco.weathr;

import android.app.Application;

/**
 * Created by pablisco on 30/05/2014.
 *
 * Generic context to access the application context outside a contextual component (Activity,
 * Service, etc).
 *
 */
public class WeathrApplication extends Application {

	private static WeathrApplication sInstance;

	@Override
	public void onCreate() {
		super.onCreate();
		// register app
		sInstance = this;
	}

	/**
	 * Static accessor, no need to lazy load since {@link #onCreate()} must be called
	 * @return
	 */
	public static WeathrApplication app() {
		return sInstance;
	}
}
