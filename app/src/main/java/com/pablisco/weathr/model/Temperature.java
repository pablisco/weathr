package com.pablisco.weathr.model;

/**
 * Created by pablisco on 28/05/2014.
 *
 * Describes the different temperatures in a forecast.
 *
 */
public interface Temperature {

	/**
	 * @return the temperature for the day
	 */
	Float getDay();

	/**
	 * @return the minimum temperature
	 */
	Float getMinimum();

	/**
	 * @return the maximum temperature
	 */
	Float getMaximum();

	/**
	 * @return the temperature at night
	 */
	Float getNight();

	/**
	 * @return the temperature in the evening
	 */
	Float getEvening();

	/**
	 * @return the temperature in the morning
	 */
	Float getMorning();

}
