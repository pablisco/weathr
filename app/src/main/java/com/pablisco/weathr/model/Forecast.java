package com.pablisco.weathr.model;

import hirondelle.date4j.DateTime;

/**
 * Created by pablisco on 28/05/2014.
 *
 * Simple description of the model for a weather forecast.
 *
 */
public interface Forecast {

	/**
	 * @return the time corresponding to the given forecast
	 */
	DateTime getTime();

	/**
	 * @return {@link Temperature} for the forecast
	 */
	Temperature getTemperature();

	/**
	 * @return Atmospheric pressure
	 */
	Float getPressure();

	/**
	 * @return wind speed
	 */
	Float getWindSpeed();

	/**
	 * @return wind direction in degrees
	 */
	Float getWindDirection();

	/**
	 * @return humidity in percentage
	 */
	Integer getHumidity();

	/**
	 * @return a list of weather conditions
	 */
	Iterable<Weather> getWeatherList();



}
