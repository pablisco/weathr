package com.pablisco.weathr.model;

/**
 * Created by pablisco on 29/05/2014.
 *
 * Description of coordinates in Earth's surface (latitude and longitude)
 *
 */
public interface Coordinates {

	/**
	 * @return the angular distance in degrees from the Earth's equator
	 */
	Float getLatitude();

	/**
	 * @return the angular distance in degrees from the Greenwich meridian
	 */
	Float getLongitude();

}
