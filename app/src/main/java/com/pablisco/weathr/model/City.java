package com.pablisco.weathr.model;

/**
 * Created by pablisco on 29/05/2014.
 *
 * Description of a city Object
 *
 */
public interface City {

	/**
	 * @return an identifier for the city
	 */
	Long getId();

	/**
	 * @return the name of the city
	 */
	String getName();

	/**
	 * @return the coordinates for the city
	 */
	Coordinates getCoordinates();

	/**
	 * @return the country of the city
	 */
	String getCountry();

}
