package com.pablisco.weathr.concurrent;

import android.os.AsyncTask;

import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.SettableFuture;
import com.pablisco.weathr.lang.Either;

/**
 * Created by pablisco on 30/05/2014.
 *
 * Util extension of {@link AsyncTask} that uses a future to handle errors.
 *
 */
public abstract class FutureTask<T, R> extends AsyncTask<T, Integer, Either<R, Throwable>> {

	private final SettableFuture<R> mFuture = SettableFuture.create();

	@SafeVarargs
	@Override
	protected final Either<R, Throwable> doInBackground(T... params) {
		Either<R, Throwable> result;
		try {
			result = Either.left(tryInBackground(params));
		} catch (Throwable t) {
			result = Either.right(t);
		}

		return result;
	}

	@Override
	protected void onPostExecute(Either<R, Throwable> either) {
		if (either.isLeft()) {
			mFuture.set(either.getLeft());
		} else {
			mFuture.setException(either.getRight());
		}
	}

	@SuppressWarnings("RedundantThrows")
	protected abstract R tryInBackground(T... params) throws Throwable;

	@SafeVarargs
	public final ListenableFuture<R> executeWithFuture(T... params) {
		execute(params);
		return mFuture;
	}

}
