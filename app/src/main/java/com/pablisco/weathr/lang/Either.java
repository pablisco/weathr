package com.pablisco.weathr.lang;

import com.google.common.base.Optional;

/**
 * Created by pablisco on 29/05/2014.
 *
 * Simple pattern to provide two values as a response of a method. It forces the consumer to check
 * these values. It's a concept that extends from the option pattern used with {@link Optional}
 *
 * TODO: Unit tests
 *
 */
public abstract class Either<L, R> {

	/**
	 * Let's make sure nobody can instantiate this :)
	 */
	private Either() {
	}

	public static <L, R> Either<L, R> left(L left) {
		return new EitherLeft<>(left);
	}

	public static <L, R> Either<L, R> right(R right) {
		return new EitherRight<>(right);
	}

	public L getLeft() {
		throw new UnsupportedOperationException();
	}

	public R getRight() {
		throw new UnsupportedOperationException();
	}

	public boolean isLeft() {
		return false;
	}

	public boolean isRight() {
		return false;
	}

	private static class EitherLeft<L, R> extends Either<L, R> {

		final L left;

		EitherLeft(L left) {
			this.left = left;
		}

		@Override
		public boolean isLeft() {
			return true;
		}

		@Override
		public L getLeft() {
			return left;
		}
	}

	private static class EitherRight<L, R> extends Either<L, R>  {

		final R right;

		EitherRight(R right) {
			this.right = right;
		}

		@Override
		public boolean isRight() {
			return true;
		}

		@Override
		public R getRight() {
			return right;
		}
	}

}
